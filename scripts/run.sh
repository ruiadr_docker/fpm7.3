#!/bin/bash

# TZ

ln -sf "/usr/share/zoneinfo/$APP_TMZ" /etc/localtime
echo "$APP_TMZ" > /etc/timezone

# Utilisateur

if [ ! -d "/home/$APP_USR" ]; then
    /usr/sbin/addgroup --gid $APP_GID $APP_GRP

    /usr/sbin/adduser --disabled-password --force-badname \
        --gecos "Utilisateur $APP_USR" \
        --gid $APP_GID --uid $APP_UID \
        --home /home/$APP_USR \
        $APP_USR

    chown -R $APP_USR:$APP_GRP /var/www/html
fi

# PHP

PHP_DIR='/usr/local/etc/php'
FPM_DIR='/usr/local/etc/php-fpm.d'

if [ "$APP_ENV" = 'dev' ]; then
    ln -sf $PHP_DIR/php.ini-development $PHP_DIR/conf.d/php.ini
    ln -sf $PHP_DIR/zzz-dev.ini $PHP_DIR/conf.d/zzz-custom.ini
else
    ln -sf $PHP_DIR/php.ini-production $PHP_DIR/conf.d/php.ini
    ln -sf $PHP_DIR/zzz-prod.ini $PHP_DIR/conf.d/zzz-custom.ini
fi

sed -i "s~__APP_USR__~$APP_USR~g" $FPM_DIR/zzz-app.conf
sed -i "s~__APP_GRP__~$APP_GRP~g" $FPM_DIR/zzz-app.conf

sed -i "s~__TIMEZONE__~$APP_TMZ~g" $PHP_DIR/zzz-common.ini
ln -sf $PHP_DIR/zzz-common.ini $PHP_DIR/conf.d/zzz-common.ini

# ... démarrage de php-fpm ...
php-fpm
